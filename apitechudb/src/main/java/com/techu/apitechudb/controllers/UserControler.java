package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class UserControler<user> {

    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<UserModel> addProduct(@RequestBody UserModel user){
        System.out.println("addProduct");
        System.out.println("La id del usuario que se vaa crear es " + user.getId());
        System.out.println("EL nombre del usuario que se va a crear es "+ user.getName());
        System.out.println("La edad del usuario que se va a crear es "+ user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user)
                , HttpStatus.CREATED
        );

    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserByid(@PathVariable String id){
        System.out.println("getUserByid");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "User no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> UpdateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("UpdateUser");
        System.out.println("La id del usuario que se va a ctualizar en parametro URL es es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId() );
        System.out.println("El nombre del usuario que se va a actualizar es "+user.getName());
        System.out.println("La edad del usuario que se va a actualizar "+ user.getAge());

        Optional<UserModel> UserToUpdate = this.userService.findById(id);

        if(UserToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado , actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>(
            user
            , UserToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id){
        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado" ,
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
