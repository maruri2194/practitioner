package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class ProductController<product> {

    @Autowired
    ProductService productService;

    @GetMapping("/product")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll()
                , HttpStatus.OK
        );

    }
            @GetMapping("/products/{id}")
            public ResponseEntity<Object> getProductByid (@PathVariable String id) {
                System.out.println("getProductByid");
                System.out.println("La id del producto a buscar es" + id);

            Optional<ProductModel> result = this.productService.findById(id);

            return new ResponseEntity<>(
                    result.isPresent() ? result.get() : "Producto no encontrado",
                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addproduct");
        System.out.println("La id del producto que se va a crear es  "+ product.getId());
        System.out.println("La descripcion del producto ques e va a crear es " + product.getDesc());
        System.out.println("El precio del producto ques e va a crear es " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> UpdateProduct(@RequestBody ProductModel product, @PathVariable String id ) {
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va a actualizar en parametro URL es  " + id);
        System.out.println("La id del producto que se va a actualizar es  " + product.getId());
        System.out.println("La descripcion del producto ques e va a actualizar es " + product.getDesc());
        System.out.println("El precio del producto ques e va a actualizar es " + product.getPrice());

        //devuelve optional
        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(product);

        }

        return new ResponseEntity<>(
                product
                //, HttpStatus.OK
                ,productToUpdate.isPresent() ? HttpStatus.OK :  HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("Deleteproduct");

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
            deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
